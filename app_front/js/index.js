$(function() {
  $('#mensagem').keypress(async function(event) {
    if (event.which == 13) {
      event.preventDefault();
      let data = $('#mensagem').val();

      if (isCommand(data)) {
        let command = String(data).trim();
        let args = command.split(' ');
        let params = data.substring(args[0].length);

        params = String(params).trim();
        //Monta jSON {"command": "/comando", "params":"parametros do comando"}
        let jsonData = { command: args[0], params };
        let resp = await getResultComand(jsonData);
        showMsg(fmtMsgEnviada(data));
        //quando os comandos executados retornam resposta (essas respostas não são do servidor EX: freenode)
        // calc, help, modes
        if (resp != '') {
          if (resp.respCommand.isarray) {
            //comandos como /help e modes retornam uma array
            mostraArray(resp.respCommand.array);
          } else {
            //mostra resultados dos comandos
            showMsg(fmtMsgServer(resp.respCommand));
          }
        }
        $('#mensagem').val('');
      } else {
        showMsg(fmtMsgEnviada(data));
        var msg = JSON.parse(
          '{"timestamp":' +
            Date.now() +
            ',' +
            '"nick":"' +
            Cookies.get('nick') +
            '",' +
            '"msg":"' +
            data +
            '"}'
        );

        let resp = await submete_mensagem(msg);
        $('#mensagem').val('');
      }

      if (data == '/quit') {
        redirectHome();
      }
    }
  });
});

function showMsg(msg) {
  $('#mural').append('<li>' + msg + '</li>');
}

function fmtMsgRecebida(timestamp, nick, msg) {
  return '[' + timestamp_to_date(timestamp) + '] - ' + nick + ': ' + msg + '';
}

function fmtMsgEnviada(msg) {
  return '[' + timestamp_to_date(Date.now()) + '] - Eu: ' + msg;
}

function fmtMsgServer(msg) {
  return '[' + timestamp_to_date(Date.now()) + '] - SERVER: ' + msg;
}

function mostraArray(array) {
  array.forEach(element => {
    showMsg(fmtMsgServer(element));
  });
}
