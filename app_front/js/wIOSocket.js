var iosocket = io.connect();
iosocket.on('connect', function() {
  //showMsg('Connected');
  showMsg('Aguardando inscrição na sala');
  iosocket.on('connected', function(data) {
    showMsg('Conectado');
  });
  iosocket.on('message', function(data) {
    showMsg(fmtMsgRecebida(data.timestamp, data.nick, data.msg));
  });

  iosocket.on('disconnect', function() {
    showMsg('Disconnected');
  });
});
