proxy = require('../proxy/proxys');
sendMSG = require('../proxy/messages');
let array; //comandos /help e /modes retornam array
let irc_client;
let aux; //utilizado para separar parametros para comandos que utilizam mais de um.

module.exports = {
  getCommand: (command, params, id) => {
    if (id) irc_client = proxy.getClient(id);

    switch (command) {
      case '/join':
        if (params[0] != '#') return 'Parâmetro inválido /join <#canal>';
        sendMSG.join(irc_client, { channel: params });
        irc_client.channels.put(params);
        return 'Entrando em ' + params;
        break;
      case '/leave':
        if (params[0] != '#') return 'Parâmetro inválido /leave <#canal>';
        sendMSG.send(irc_client, 'PART', { channel: params });
        return 'Saindo de ' + params;
        break;
      case '/nick':
        return sendMSG.send(irc_client, 'NICK', { newNick: params });
        break;
      case '/mode':
        let { target, args } = params;

        if (target) {
          mode = args;
        } else {
          aux = params.trim();
          aux = params.split(' ');
          mode = aux[0];
          if (aux[1]) {
            target = aux[1];
          } else {
            target = irc_client.opt.channels[0];
          }
        }
        sendMSG.send(irc_client, 'MODE', { mode, target, by: irc_client.nick });
        return 'colocando o modo ' + mode + ' em ' + target;
        return;
        break;
      case '/calc':
        let result;
        if (params) {
          try {
            result = eval(params);
          } catch (err) {
            result = 'Parametros inválidos';
          }
        } else {
          result = 'Parâmetro vazio';
        }
        return result;
        break;
      case '/motd':
        sendMSG.send(irc_client, 'MOTD');
        return 'Buscando MOTD';
        break;
      case '/modes':
        array = require('../lists/listModes');
        return {
          isarray: true,
          array
        };
        break;
      case '/help':
        array = require('../lists/listCommands')(params);
        if (params) {
          if (array.length > 1) {
            return {
              isarray: true,
              array
            };
          } else {
            return array;
          }
        } else {
          return {
            isarray: true,
            array
          };
        }
        break;
      case '/privmsg':
        aux = params.split(' ');
        let to = aux[0];
        let msg = aux[1];
        irc_client = proxy.getClient(id);
        sendMSG.say(irc_client, to, msg);
        return 'enviando privmsg';
        break;
      case '/quit':
        irc_client = proxy.getClient(id);
        sendMSG.send(irc_client, 'QUIT', { nick: irc_client.nick });
        break;
      case '/kick':
        aux = params.split(' ');
        irc_client = proxy.getClient(id);
        sendMSG.send(irc_client, 'KICK', { target: aux[0], canal: aux[1] });
        return 'Retirando ' + aux[0] + ' do canal ' + aux[1];
        break;
      default:
        return 'Comando ' + command + ' inválido';
        break;
    }
  }
};
