sendMSG = require('./messages');

module.exports = (irc_client, ws) => {
  //To pode ser um canal ou um usuário no caso de mensagem privada
  irc_client.addListener('message', (from, to, message) => {
    sendMessage(ws, from, message, to);
  });

  //Algum usuário alterou o nickname
  irc_client.addListener('nick', (nickAnt, nickAtual) => {
    sendMessage(ws, 'SERVER', nickAnt + ' agora é ' + nickAtual);
  });

  //alguém entrou no canal
  irc_client.addListener('join', (channel, nick) => {
    if (irc_client.nick == nick) {
      nick = 'Você';
    }
    sendMessage(ws, 'SERVER', nick + ' entrou na sala ' + channel);
  });

  //Alguém desconectou do servidor
  irc_client.addListener('quit', (nick, reason) => {
    if (reason) {
      sendMessage(ws, 'SERVER', nick + ' desconectou :' + reason);
    } else {
      sendMessage(ws, 'SERVER', nick + ' desconectou');
    }
  });

  //Alguém sai do canal
  irc_client.addListener('part', (channel, nick, reason) => {
    sendMessage(ws, 'SERVER', nick + ' deixou a sala ' + channel);
  });

  //Alguém foi tirado do canal
  irc_client.addListener('kick', (channel, nick, by) => {
    sendMessage(ws, 'SERVER', by + ' expulsou ' + nick + ' da sala ' + channel);
  });

  //Alguém acrescentou modo no usuário ou canal
  irc_client.addListener('+mode', (channel, by, mode, user) => {
    sendMessage(
      ws,
      'SERVER',
      (by ? by : 'Você') +
        ' colocou o modo +' +
        mode +
        ' em ' +
        (user ? user : channel)
    );
  });

  //Alguém removeu modo no usuário ou canal
  irc_client.addListener('-mode', (channel, by, mode, user) => {
    sendMessage(
      ws,
      'SERVER',
      by + ' colocou o modo -' + mode + ' em ' + (user ? user : channel)
    );
  });

  //Servidor emitiu MOT
  irc_client.addListener('motd', motd => {
    let motdfmt = formatMOTD(motd);
    sendMessage(ws, 'SERVER', motdfmt);
  });

  //Servidor emitiu código de erro
  //Mensagens de erro no arquivo listError
  irc_client.addListener('error', message => {
    sendMessage(
      ws,
      'SERVER',
      'ERRO: ' + require('../lists/listError')(parseInt(message.rawCommand))
    );
  });
};

sendMessage = (ws, from, msg, to) => {
  let mesage;
  //se o destino da mensagem não for undefinned e não for pra sala a mensagem é privada
  if (to != undefined && to[0] != '#') {
    mesage = {
      timestamp: Date.now(),
      nick: 'Mensagem privada de ',
      msg: from + ': ' + msg
    };
  } else {
    mesage = {
      timestamp: Date.now(),
      nick: from,
      msg: msg
    };
  }
  console.log(mesage.nick + ' :' + (to ? to + ' => ' : ' ') + mesage.msg);
  sendMSG.emmit(ws, 'message', mesage);
};

formatMOTD = motd => {
  let data = motd.split('- ');
  let msg = '';
  data.forEach(element => {
    msg += '- ' + element + '<br>';
  });
  return msg;
};
