module.exports = {
  send: (irc_client, msg, params) => {
    switch (msg) {
      case 'NICK':
        try {
          irc_client.send(msg, params.newNick);
          return true;
        } catch (error) {
          return false;
        }
        break;
      case 'PART':
        irc_client.send(msg, params.channel);
        break;
      case 'QUIT':
        irc_client.send(msg, params.nick);
        break;
      case 'KICK':
        irc_client.send(msg, params.canal, params.target);
        break;
      case 'MODE':
        irc_client.send(msg, params.target, params.mode, params.by);
        break;

      case 'MOTD':
        irc_client.send(msg);
        break;
    }
  },

  //target pode ser #canal ou usuário no caso de privmsg
  say: (irc_client, target, msg) => {
    irc_client.say(target, msg);
  },

  join: (irc_client, params) => {
    irc_client.join(params.channel);
  },

  emmit: (socket, type, data) => {
    socket.emit(type, data);
  }
};
