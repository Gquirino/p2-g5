let path = require('path');
let proxy = require('../proxy/proxys');
let commands = require('../commands/commands');
let sendMSG = require('../proxy/messages');
module.exports = {
  home: (req, res) => {
    let socketio = require('../../app');
    let { servidor, nick, canal } = req.cookies;
    if (servidor && nick && canal) {
      let proxy_id = proxy.getProxyId();
      socketio.on('connection', socket => {
        var p = proxy.proxy(proxy_id, servidor, nick, canal, socket);
      });
      res.sendFile(path.join(__dirname, '../../app_front/view/index.html'));
    } else {
      res.sendFile(
        path.join(__dirname, '../../app_front/view/login/login.html')
      );
    }
  },
  gravarMensagem: (req, res) => {
    let { timestamp, nick, msg } = req.body;
    let { id, canal } = req.cookies;
    irc_client = proxy.getClient(id);

    console.log(nick + ' : ' + canal + ' => ' + msg);

    sendMSG.say(irc_client, canal, msg);
    res.send({ respCommand: '' });
  },
  login: (req, res) => {
    let { nome, canal, servidor } = req.body;
    res = createCookie(nome, canal, servidor, res, proxy.getProxyId());
    res.redirect('/');
  },
  resultCommand: (req, res) => {
    let respCommand;
    let { command, params } = req.body;
    let { id, nick, canal, servidor } = req.cookies;
    if (req.params.mode) {
      command = '/mode';
      if (!params) {
        params = {
          target: nick,
          args: req.params.args
        };
      } else {
        params = String(params).trim();
      }
    }
    if (command == '/nick') {
      //Se retornar true, o comando nick foi executado com sucesso e pode alterar o cookie
      if (commands.getCommand(command, params, id)) {
        res = updateCookie(params, canal, servidor, res, proxy.getClient(id));
        res.send({ respCommand: 'alterando o nickname' });
      }
    } else if (command == '/quit') {
      deleteCookie(res);
      commands.getCommand(command, params, id);
      res.send({ respCommand: 'saindo do servidor' });
    } else {
      respCommand = commands.getCommand(command, params, id);
      res.send({ respCommand });
    }
  },
  mode: (req, res) => {
    let { id, nick } = req.cookies;
    if (req.params) {
      let { args, target } = req.params;
      if (!target) {
        target = nick;
      }
      let params = {
        args,
        target
      };
    } else {
      let { params } = req.body;
    }
    let { command } = req.body;

    respCommand = commands.getCommand(command, params, id);
    res.send(retorno);
  }
};

function createCookie(nome, canal, servidor, res, id) {
  res.cookie('nick', nome, {
    maxAge: 24 * 60 * 60 * 1000,
    httpOnly: true
  });
  res.cookie('canal', canal, {
    maxAge: 24 * 60 * 60 * 1000,
    httpOnly: true
  });
  res.cookie('servidor', servidor, {
    maxAge: 24 * 60 * 60 * 1000,
    httpOnly: true
  });
  res.cookie('id', id, {
    maxAge: 24 * 60 * 60 * 1000,
    httpOnly: true
  });
  proxy.incrementIdProxy();
  return res;
}

function updateCookie(nome, canal, servidor, res, irc) {
  irc.nick = nome;
  res.cookie('nick', nome);
  res.cookie('canal', canal);
  res.cookie('servidor', servidor);
  return res;
}

function deleteCookie(res) {
  res.clearCookie('id');
  res.clearCookie('nick');
  res.clearCookie('canal');
  res.clearCookie('servidor');
  res.clearCookie('io');
  return res;
}
