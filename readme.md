# Projeto 2

Cliente web para chat IRC da disciplina de aplicações/sistemas distribuídos 2018-1

## Alunos

* Daniel Cavalcante
* Gilmar Bernardes
* Guilherme Paniago
* Guilherme Quirino de Melo - Dev

### Andamento do projeto:

**Proxy**

* Listeners implementados para tratar as mensagens recebidas do server:<br />
  O servidor envia alguma(s) dessas mensagens sempre que há atividade no canal.<br />
  * 'message': Alguma mensagem é recebida no chat do server<br />
  * 'nick': Usuário altera o nickname,<br />
  * 'join': Usuário entra no canal,<br />
  * 'quit': Usuário sai do server,<br />
  * 'part': Usuário sai do canal,<br />
  * 'kick': Usuário é expulso do canal,<br />
  * '+mode': Usuário coloca determinado modo em canal ou usuário,<br />
  * '-mode': Usuário retira determinado modo em canal ou usuário,<br />
  * 'motd': Servidor envia motd ao cliente,<br />
  * 'error': Servidor envia mensagem de erro.<br />

**WebSocket**

* Enviar e receber mensagens do canal implementado<br />
* Comandos que o cliente pode executar implementados:<br />

  **/join**: entrar em um canal<br />
  `/join <#canal>`<br />
  `#canal: nome do canal para entrar`<br />

  **/leave**: sair do canal<br />
  `/leave <#canal>`<br />
  `#canal: nome do canal para sair`<br />

  **/nick**: alterar o nickname<br />
  `/nick <nickname>`<br />
  `nickname: nome do nickname para alterar`<br />

  **/mode**: trocar modos do usuário ou canal<br />
  `/mode <modo> <alvo>`<br />
  `modo: tipo de modo a ser alterado`<br />
  `<+modo> acrescenta modo informado`<br />
  `<-modo> remove modo informado`<br />
  `alvo: indica a quem deve ser alterado o modo, #canal ou usuário`<br />
  `alvo: se esse valor for omitido, será considerado #canal`<br />

  **/privmsg**: Enviar mensagem privada<br />
  `/privmsg <nickname> <mensagem>`<br />
  `nickname: nome do nickname do usuário a enviar a mensagem privada`<br />

  **/help**: vizualizar os comandos suportados<br />
  `/help`<br />
  `/help <comando> - buscar unfirmações do comando`<br />

  **/modes**: visualizar os modos disponíveis<br />
  `/modes`<br />

  **/nick**: alterar o nickname<br />
  `/nick <nickname>`<br />
  `#nickname: nome do nickname para alterar`<br />

  **/quit**: desconecta do servidor<br />
  `/quit`

  **/kick**: retirar usuário do canal (Requer privilégios de operador)<br />
  `/kick <usuário> <#canal>`

  **/calc** (exercício realizado em sala de aula): resolve expressões numéricas<br />
  `/calc <expressão_numerica>`

---

**Erros comuns**

```
|   Código  |       Mensagem            |
|    403    | CANAL INEXISTENTE         |
|    433	| NICKNAME EM USO           |
|    461	| PARAMETROS INSUFICIENTES  |
```

**Modos mais comuns**

```
| Modo  |			Alvo: Canal			    |    Alvo: Usuário  |
|  a    |               -                   |  Torna Operador  |
|  b    |               -                   |   Banir do canal  |
|  i    |       Apenas para convidados      |  Torma invisível  |
|  m    |   Apenas moderadores podem falar  |  Torna Operador  |
|  o    |               -                   |  Torna Operador  |
|  p    |          Canal privado            |         -         |
```
